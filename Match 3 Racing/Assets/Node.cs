﻿using UnityEngine;

public class Node : MonoBehaviour
{
    // Use this for initialization
    void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Node entered!");
        var vehicleAi = other.GetComponent<VehicleAI>();
        if (vehicleAi != null)
        {
            vehicleAi.EnteredNode(GetComponent<Transform>());
        }
    }
}
