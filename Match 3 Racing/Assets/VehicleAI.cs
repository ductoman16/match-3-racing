﻿using UnityEngine;

public class VehicleAI : MonoBehaviour
{
    public Path Path;
    public float Speed = .2f;
    public int TargetOffsetMax = 10;

    public int TargetNodeIndex;

    public Transform Avoid;
    private Vector3 _targetOffset;

    void Update()
    {
        if (Avoid != null)
        {
            var difference = transform.position - Avoid.position;
            var inverseDifference = 1 / difference.magnitude;
            transform.position = Vector3.MoveTowards(transform.position, Avoid.position, inverseDifference * -5 * Speed);
        }
        else
        {
            var targetPosition = GetTargetNodePosition();
            transform.position = Vector3.MoveTowards(transform.position, targetPosition, Speed);

            RotateTowards(targetPosition);
        }
    }

    private Vector3 GetTargetNodePosition()
    {
        var targetNode = Path.Nodes[TargetNodeIndex];
        var targetWithOffsett = targetNode.position + _targetOffset;
        return targetWithOffsett;
    }

    private void RotateTowards(Vector3 targetPosition)
    {
        var direction = targetPosition - transform.position;
        direction.y = 0;
        var rotation = Quaternion.LookRotation(direction);
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, 0.1f);
    }

    //void OnTriggerEnter(Collider other)
    //{
    //    Debug.Log("Trigger Enter!");
    //    if (ReferenceEquals(other.gameObject, Path.Nodes[TargetNodeIndex].gameObject))
    //    {
    //        TargetNextNode();
    //    }
    //}

    public void TargetNextNode()
    {
        TargetNodeIndex = Path.GetNextIndex(TargetNodeIndex);
        _targetOffset = new Vector3(
            Random.Range(-TargetOffsetMax, TargetOffsetMax),
            0,
            Random.Range(-TargetOffsetMax, TargetOffsetMax));
        Debug.Log(_targetOffset);
    }


    public void EnteredNode(Transform node)
    {
        //If we've entered the node we're targeting
        if (ReferenceEquals(node, Path.Nodes[TargetNodeIndex]))
        {
            TargetNextNode();
        }
    }
}
