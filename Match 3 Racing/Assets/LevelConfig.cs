﻿using System;
using UnityEngine;

namespace Assets
{
    [Serializable]
    public class LevelConfig
    {
        public Color[] Colors;
        public int SetsPerColor = 3;
        public Path Path;
    }
}
