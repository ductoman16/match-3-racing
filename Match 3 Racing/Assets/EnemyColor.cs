﻿using UnityEngine;

public class EnemyColor : MonoBehaviour
{
    public Color Color;

    // Use this for initialization
    void Start()
    {
        GetComponent<Renderer>().material.color = Color;
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnCollisionEnter(Collision collision)
    {
        //Debug.Log("Enemy collide");
    }

    public void Matched()
    {
        Destroy(gameObject);
    }
}
