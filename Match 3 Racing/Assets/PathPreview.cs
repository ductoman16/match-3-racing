﻿using UnityEngine;

public class PathPreview : MonoBehaviour
{
    public Color LineColor;

    void OnDrawGizmos()
    {
        Gizmos.color = LineColor;

        var nodes = GetComponent<Path>().Nodes;

        foreach (var node in nodes)
        {
            Gizmos.DrawWireSphere(node.position, 1f);
        }
    }
}
