﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    public Button StartButton;
    public InputField LevelSelector;

    public static int SelectedLevel = 1;

    // Use this for initialization
    void Start()
    {
        StartButton.onClick.AddListener(() =>
        {
            try
            {
                SelectedLevel = Convert.ToInt32(LevelSelector.text);
            }
            catch (FormatException)
            {
                SelectedLevel = 1;
            }
            SceneManager.LoadScene("Main");
        });
    }

    // Update is called once per frame
    void Update()
    {

    }
}
