﻿using System.Collections.Generic;
using System.Linq;
using Assets;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public GameObject EnemyPrefab;
    public GameObject PlayerPrefab;
    public float SpawnOffset = 10f;

    public LevelConfig[] LevelConfigs;

    //public List<LevelConfig> LevelConfigList = new List<LevelConfig>()
    //{
    //    new LevelConfig()
    //    {
    //        Colors = new[]{Color.red, Color.green, Color.blue},
    //        Path 
    //    }
    //};

    private ICollection<GameObject> _enemyInstances;
    public PlayerController PlayerInstance { get; private set; }
    private int _levelCounter;

    public GameState GameState { get; private set; }

    // Use this for initialization
    void Start()
    {
        _levelCounter = MenuController.SelectedLevel - 1;
        StartLevel(LevelConfigs[_levelCounter]);
    }

    void Update()
    {
        if (PlayerInstance != null)
        {
            if (PlayerInstance.IsDead && GameState == GameState.Started)
            {
                GameState = GameState.GameOver;
            }
        }

        if (_enemyInstances.All(e => e == null) && GameState == GameState.Started)
        {
            if (_levelCounter == LevelConfigs.Length - 1)
            {
                GameState = GameState.AllLevelsComplete;
            }
            else
            {
                GameState = GameState.LevelComplete;
            }
        }
    }

    private void StartLevel(LevelConfig levelConfig)
    {
        SpawnPlayer(levelConfig);
        SpawnEnemies(levelConfig);
    }

    private void SpawnPlayer(LevelConfig levelConfig)
    {
        if (PlayerInstance != null)
        {
            Destroy(PlayerInstance.gameObject);
        }

        var startLocation = levelConfig.Path.Nodes[0].position;
        var gameOjbect = Instantiate(PlayerPrefab, startLocation, Quaternion.identity);
        PlayerInstance = gameOjbect.GetComponent<PlayerController>();
    }

    private void SpawnEnemies(LevelConfig levelConfig)
    {
        _enemyInstances = new List<GameObject>();

        foreach (var color in levelConfig.Colors)
        {
            for (int i = 0; i < levelConfig.SetsPerColor; i++)
            {
                SpawnEnemy(levelConfig.Path, color);
                SpawnEnemy(levelConfig.Path, color);
                SpawnEnemy(levelConfig.Path, color);
            }
        }
    }

    private void SpawnEnemy(Path path, Color color)
    {
        var nodeIndex = Random.Range(1, path.Nodes.Length - 1);
        var position = path.Nodes[nodeIndex].transform.position;

        position.x += Random.Range(0, SpawnOffset);
        position.z += Random.Range(0, SpawnOffset);
        position.y = EnemyPrefab.transform.position.y;

        var enemy = Instantiate(EnemyPrefab.gameObject, position, Quaternion.identity);
        enemy.GetComponent<EnemyColor>().Color = color;

        var vehicleAi = enemy.GetComponent<VehicleAI>();
        vehicleAi.Path = path;
        vehicleAi.TargetNodeIndex = nodeIndex;

        _enemyInstances.Add(enemy);
    }

    public void LoadNextLevel()
    {
        _levelCounter++;
        StartLevel(LevelConfigs[_levelCounter]);
        GameState = GameState.Started;
    }

    public void RetryLevel()
    {
        foreach (var enemyInstance in _enemyInstances)
        {
            Destroy(enemyInstance);
        }
        StartLevel(LevelConfigs[_levelCounter]);
        GameState = GameState.Started;
    }
}

public enum GameState
{
    Started,
    LevelComplete,
    GameOver,
    AllLevelsComplete
}
