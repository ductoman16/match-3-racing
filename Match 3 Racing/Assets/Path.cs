﻿using System.Linq;
using UnityEngine;

public class Path : MonoBehaviour
{
    public Transform[] Nodes
    {
        get
        {
            return GetComponentsInChildren<Transform>().Where(t => t != transform).ToArray();
        }
    }

    public int GetNextIndex(int targetNodeIndex)
    {
        if (targetNodeIndex == Nodes.Length - 1)
        {
            return 0;
        }
        return targetNodeIndex + 1;
    }
}
