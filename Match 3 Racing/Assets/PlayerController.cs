﻿using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private const string SlowzoneTag = "SlowZone";
    public float speed = 1F;
    public float rotationSpeed = 100.0F;
    private Rigidbody _rigidbody;

    private int _matchCount;
    private int _requiredMatches = 3;

    public float InitialHealth = 5;
    public float CurrentHealth { get; private set; }

    public float BoostMultiplier = 2;
    public float BoostMax = 100;
    public float BoostRemaining { get; private set; }

    public float SlowZoneFactor = .5f;
    private float _currentSlowFactor = 1;

    public Color currentColor = Color.white;

    public bool Started { get; private set; }

    public bool IsDead
    {
        get { return Started && CurrentHealth <= 0; }
    }

    void Start()
    {
        CurrentHealth = InitialHealth;
        BoostRemaining = BoostMax;
        _rigidbody = GetComponent<Rigidbody>();
        Started = true;
    }

    void Update()
    {
        GetComponent<Renderer>().material.color = currentColor;

        HandleInput();
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;

        Gizmos.DrawRay(transform.position, transform.forward * 10);
    }

    private void HandleInput()
    {
        var currentBoost = GetBoost();
        float thrust = Input.GetAxis("Vertical") * speed * currentBoost * _currentSlowFactor;
        float horizontal = Input.GetAxis("Horizontal") * rotationSpeed;

        //Debug.Log(transform.forward);

        //_rigidbody.velocity = new Vector3(thrust, thrust, thrust);

        transform.Translate(-transform.forward * thrust, Space.World);
        transform.Rotate(new Vector3(0, 1, 0) * Time.deltaTime * horizontal, Space.World);

        //transform.Rotate(new Vector3(0, -1, 0) * horizontal, Space.World);
        //transform.position = Vector3.MoveTowards(transform.position, transform.forward, thrust);
        //_rigidbody.AddRelativeForce(thrust, 0, 0);
        //_rigidbody.AddRelativeTorque(0, horizontal, 0);
    }

    private float GetBoost()
    {
        if (Input.GetKey(KeyCode.Space) && BoostRemaining > 0)
        {
            BoostRemaining -= 2;
            return BoostMultiplier;
        }

        if (BoostRemaining < BoostMax)
        {
            BoostRemaining++;
        }

        return 1;
    }

    void OnCollisionEnter(Collision collision)
    {
        var enemy = collision.gameObject.GetComponent<EnemyColor>();
        if (enemy != null)
        {
            HandleColorCollision(enemy);
        }


    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == SlowzoneTag)
        {
            _currentSlowFactor = SlowZoneFactor;
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.tag == SlowzoneTag)
        {
            _currentSlowFactor = 1;
        }
    }

    private void HandleColorCollision(EnemyColor enemy)
    {
        //Debug.Log(enemy.Color);
        //Debug.Log(currentColor);

        if (currentColor == Color.white)
        {
            currentColor = enemy.Color;
        }

        if (currentColor == enemy.Color)
        {
            enemy.Matched();
            _matchCount++;
        }
        else
        {
            TakeDamage();
        }

        if (_matchCount >= _requiredMatches)
        {
            MatchSetCompleted();
        }
    }

    private void TakeDamage()
    {
        CurrentHealth--;
    }

    private void MatchSetCompleted()
    {
        currentColor = Color.white;
        _matchCount = 0;
    }

    void LateUpdate()
    {
        ClampRotation();
    }

    private void ClampRotation()
    {
        //Debug.Log(transform.localEulerAngles);
        //transform.localEulerAngles = new Vector3(
        //    90,
        //    transform.localEulerAngles.y,
        //    0);
    }
}
