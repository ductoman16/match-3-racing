﻿using UnityEngine;

public class PersonalSpaceController : MonoBehaviour
{
    public float MaxAdjustSpeed = 2;
    private VehicleAI _thisVehicleAi;

    void Start()
    {
        _thisVehicleAi = GetComponentInParent<VehicleAI>();
    }

    void OnTriggerEnter(Collider other)
    {
        var otherVehicle = other.GetComponentInParent<VehicleAI>();
        if (otherVehicle != null)
        {
            //Debug.Log("Personal Space!");
            _thisVehicleAi.Avoid = other.transform;

            //var parentTransform = GetComponentInParent<Transform>();
            //parentTransform.position = Vector3.MoveTowards(parentTransform.position, other.transform.position, -MaxAdjustSpeed);
        }
        else
        {
            _thisVehicleAi.Avoid = null;
        }
    }

    void OnTriggerExit(Collider other)
    {
        _thisVehicleAi.Avoid = null;
    }
}
