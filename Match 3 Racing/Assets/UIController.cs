﻿using UnityEngine;
using UnityEngine.Experimental.UIElements;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Button = UnityEngine.UI.Button;
using Image = UnityEngine.Experimental.UIElements.Image;
using Slider = UnityEngine.UI.Slider;

public class UIController : MonoBehaviour
{
    public GameObject GameOverDisplay;
    public GameObject LevelCompleteDisplay;
    public GameObject AllLevelsCompleteDisplay;
    public Text HealthText;
    public Text BoostText;
    public Button RestartButton;
    public Button NextLevelButton;
    public Button ExitButton;
    public Button QuitButton;

    public Slider HealthSlider;
    public Slider BoostSlider;

    public GameController GameController;

    // Use this for initialization
    void Start()
    {
        RestartButton.onClick.AddListener(() =>
        {
            GameOverDisplay.SetActive(false);
            GameController.RetryLevel();
        });
        NextLevelButton.onClick.AddListener(() =>
        {
            GameController.LoadNextLevel();
            LevelCompleteDisplay.SetActive(false);
        });
        QuitButton.onClick.AddListener(() =>
        {
            SceneManager.LoadScene("Menu");
        });
        ExitButton.onClick.AddListener(() =>
        {
            SceneManager.LoadScene("Menu");
        });
    }

    // Update is called once per frame
    void Update()
    {
        HealthText.text = GameController.PlayerInstance.CurrentHealth.ToString();
        BoostText.text = GameController.PlayerInstance.BoostRemaining.ToString();

        var playerHealthPercent = GameController.PlayerInstance.CurrentHealth / GameController.PlayerInstance.InitialHealth;
        HealthSlider.value = playerHealthPercent;

        var boostPercent = GameController.PlayerInstance.BoostRemaining / GameController.PlayerInstance.BoostMax;
        BoostSlider.value = boostPercent;

        if (GameController.GameState == GameState.GameOver)
        {
            GameOverDisplay.SetActive(true);
        }

        if (GameController.GameState == GameState.AllLevelsComplete)
        {
            AllLevelsCompleteDisplay.SetActive(true);
        }

        if (GameController.GameState == GameState.LevelComplete)
        {
            LevelCompleteDisplay.SetActive(true);
        }
    }
}
